//
//  MovieCell.swift
//  MoviesApp
//
//  Created by Daniyal on 05.02.2021.
//

import UIKit

class MovieCell: UITableViewCell {
    
    public static let identifier: String = "MovieCell"

    @IBOutlet weak var ratingContainerView: UIView!
    @IBOutlet weak var overallRatingLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var originalTitleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        posterImageView.layer.cornerRadius = 8
        posterImageView.layer.masksToBounds = true
        
        ratingContainerView.layer.cornerRadius = 20
        ratingContainerView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
