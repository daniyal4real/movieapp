//
//  MovieDetailsDescriptionCell.swift
//  MoviesApp
//
//  Created by Daniyal on 11.02.2021.
//

import UIKit

class MovieDetailsDescriptionCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    public static let identifier: String = "MovieDetailsDescriptionCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
