//
//  MovieDetailsImageCell.swift
//  MoviesApp
//
//  Created by Daniyal on 11.02.2021.
//

import UIKit

class MovieDetailsImageCell: UITableViewCell {

    public static let identifier: String = "MovieDetailsImageCell"
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var ratingContainerView: UIView!
    @IBOutlet weak var overallRatingLabel: UILabel!
    @IBOutlet weak var originalTitleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        ratingContainerView.layer.cornerRadius = 20
        ratingContainerView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
