//
//  MovieDetailsViewController.swift
//  MoviesApp
//
//  Created by Daniyal on 05.02.2021.
//

import UIKit
import Alamofire
import Kingfisher

class MovieDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var movieId: Int = 0
    
    private let MOVIE_DETAILS_URL: String = "https://api.themoviedb.org/3/movie"
    private let API_KEY: String = "71c5f8ef30fef8f804a20450b817bb2b"
    
    private var movieDetails: [TrendingMoviesEntity.Movie] = [] {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: MovieDetailsImageCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: MovieDetailsImageCell.identifier)
        tableView.register(UINib(nibName: MovieDetailsDescriptionCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: MovieDetailsDescriptionCell.identifier)
        
        getMovieDetails(movieId: movieId)
    }
    
    func getMovieDetails(page: Int? = nil, movieId: Int){
        let params: [String: Any] = ["{movie_id}" : movieId, "api_key" : API_KEY]
        AF.request(MOVIE_DETAILS_URL, method: .get, parameters: params).responseJSON {(response) in
            switch response.result {
            case .success(_):
                do {
                    if let data = response.data {
                        let json = try JSONDecoder().decode(TrendingMoviesEntity.self, from: data)
                        self.movieDetails += json.results
                    }
                }
                catch let movieError {
                    print(movieError)
                }
                print(response)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieDetailsImageCell.identifier, for: indexPath) as! MovieDetailsImageCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieDetailsDescriptionCell.identifier) as! MovieDetailsDescriptionCell
        return cell
    }

    
    
}
